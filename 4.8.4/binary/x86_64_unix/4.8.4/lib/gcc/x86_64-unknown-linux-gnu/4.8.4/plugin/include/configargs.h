/* Generated automatically. */
static const char configuration_arguments[] = "../gcc-4.8.4/configure CC=/Scr/scr-test-steven/install/gcc/4.7.4/bin/gcc CXX=/Scr/scr-test-steven/install/gcc/4.7.4/bin/g++ CPPFLAGS=-I/Scr/scr-test-steven/install/gcc/4.7.4/include LDFLAGS=-L/Scr/scr-test-steven/install/gcc/4.7.4/lib64 --prefix=/Scr/scr-test-steven/install/gcc/4.8.4 --disable-multilib --with-gmp=/Scr/scr-test-steven/install/libgmp/4.3.2 --with-mpfr=/Scr/scr-test-steven/install/libmpfr/2.4.2 --with-mpc=/Scr/scr-test-steven/install/libmpc/0.8.1 --with-cloog=/Scr/scr-test-steven/install/libcloog/0.18.0 --with-isl=/Scr/scr-test-steven/install/libisl/0.11.1";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "x86-64" } };
